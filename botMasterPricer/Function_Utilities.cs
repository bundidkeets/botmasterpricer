﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace botMasterPricer
{
    class Function_Utilities
    {
        public DataTable getResultFromQuery(string commandstring, string connectionString)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                ds = queryDatabase(commandstring, connectionString);
                dt = ds.Tables[0];
                return dt;
            }
            catch
            {
                return null;
            }
        }
        private DataSet queryDatabase(string querystr, string connectionString)
        {
            string connectionstring = connectionString;
            using (SqlConnection SqlConn = new SqlConnection(connectionstring))
            {
                SqlCommand sqlCmd = new SqlCommand(querystr, SqlConn);
                sqlCmd.CommandTimeout = 0;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                using (SqlDataAdapter da = new SqlDataAdapter(sqlCmd))
                {
                    da.Fill(ds, "tables");

                }
                return ds;
            }
        }
        public void updateData(string commandString, string connectionString)
        {
            if (commandString.Length == 0) return;

            string conn = connectionString;
            using (SqlConnection SqlConn = new SqlConnection(conn))
            {
                SqlCommand sqlCmd = new SqlCommand(commandString, SqlConn);

                using (SqlDataAdapter da = new SqlDataAdapter(sqlCmd))
                {
                    sqlCmd.CommandTimeout = 0;
                    SqlConn.Open();
                    da.UpdateCommand = SqlConn.CreateCommand();
                    da.UpdateCommand = sqlCmd;
                    da.UpdateCommand.ExecuteNonQuery();
                    SqlConn.Close();
                }
            }
        }
        public List<pricetobeat> convertPriceToBeat(DataTable dt)
        {
            List<pricetobeat> prArray = new List<pricetobeat>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                pricetobeat prtobeat = new pricetobeat();
                prtobeat.city = dt.Rows[i]["City"].ToString();
                prtobeat.price = dt.Rows[i]["price"].ToString();
                prtobeat.type = dt.Rows[i]["type"].ToString();
                prtobeat.cabin = dt.Rows[i]["cabin"].ToString();
                prArray.Add(prtobeat);
            }

            return prArray;
        }
        public MPConfig getBotMasterPricerConfig(DateTime today, string connectionstring)
        {
            DataTable dt = getResultFromQuery("SELECT * FROM [Bot_MasterPricer_Config]", connectionstring);
            if (dt.Rows.Count > 0)
            {
                MPConfig conf = new MPConfig();
                conf.noOfAdults = Convert.ToInt32(dt.Rows[0]["noOfAdults"].ToString());
                conf.searchMonth = today.AddMonths(Convert.ToInt32(dt.Rows[0]["search_month"].ToString())).ToString("yyyy-MM-dd");
                conf.holiday = today.AddMonths(2).AddDays(Convert.ToInt32(dt.Rows[0]["holiday_day"].ToString())).ToString("yyyy-MM-dd");
                return conf;
            }
            else
            {
                return new MPConfig();
            }
        }
        public void setCultureInfowithPattern(string pattern, string cultureCode)
        {
            System.Globalization.CultureInfo customCulture = new System.Globalization.CultureInfo(cultureCode, true);
            customCulture.DateTimeFormat.ShortDatePattern = pattern;
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = customCulture;
        }
        public void updateMPTax(string connectionstring)
        {
            try
            {
                string command = @"DECLARE @MyCursor CURSOR;
                                DECLARE @MyCursorairport CURSOR;
                                declare @id int;
                                declare @airline nvarchar(255);
                                declare @farebasis nvarchar(255);
                                declare @rbd nvarchar(255);
                                declare @discount int;
                                declare @faretype nvarchar(255);
                                declare @trip nvarchar(255);
                                declare @departcountry nvarchar(255);
                                declare @destination nvarchar(255);
                                declare @ptc nvarchar(255);
                                declare @datecreated datetime;
                                declare @dateupdate datetime;
                                declare @city nvarchar(255);
                                declare @city_airportwithcity nvarchar(255);
                                declare @count int;
                                declare @airportcode nvarchar(255);
                                declare @row int;
                                DECLARE @SomeString  varchar(8000) = ''

                                BEGIN
	                                SET @MyCursor = CURSOR FOR select id,Airline,FareBasis,RBD,Discount,FareType,Trip,DepartCountry,Destination,Ptc,Datecreated,Dateupdate,city from dbo.Bot_MasterPricer_tax
                                    OPEN @MyCursor 
                                    FETCH NEXT FROM @MyCursor 
                                    INTO @id,@airline,@farebasis,@rbd,@discount,@faretype,@trip,@departcountry,@destination,@ptc,@datecreated,@dateupdate,@city
	                                WHILE @@FETCH_STATUS = 0
                                    BEGIN
		                                if(len(@destination) =2)
		                                begin
			                                 SET @MyCursorairport = CURSOR FOR select AirportCode from dbo.Bot_MasterPricer_Airport_City where CountryCode=@destination
				                                OPEN @MyCursorairport 
				                                FETCH NEXT FROM @MyCursorairport INTO @airportcode
				                                set @row = 1;
				                                WHILE @@FETCH_STATUS = 0
				                                BEGIN
					                                if(@row=1)
					                                begin
						                                set @SomeString += @airportcode
					                                end
					                                else
					                                begin
						                                SET @SomeString += ','+@airportcode;
					                                end
					                                set @row += 1
					                                FETCH NEXT FROM @MyCursorairport INTO @airportcode
				                                END 
				                                CLOSE @MyCursorairport ;
				                                DEALLOCATE @MyCursorairport;
				                                set @row = null
			                                update dbo.Bot_MasterPricer_tax set city=@SomeString where ID=@id 
			                                set @SomeString = ''
		                                end
		                                else
		                                begin
			                                set @destination = PARSENAME(REPLACE(@destination,',','.'),1)
			                                if(@destination ='*')
			                                begin
				                                set @city_airportwithcity ='*'
				                                update dbo.Bot_MasterPricer_tax set city=@city_airportwithcity where ID=@id
			                                end
			                                else
			                                begin
				                                set @city_airportwithcity = (select top 1 CityCode from dbo.Bot_MasterPricer_Airport_City where AirportCode=@destination or CityCode=@destination)
				                                update dbo.Bot_MasterPricer_tax set city=@city_airportwithcity where ID=@id 
			                                end
		                                end
		                                print @city_airportwithcity
		                                FETCH NEXT FROM @MyCursor INTO @id,@airline,@farebasis,@rbd,@discount,@faretype,@trip,@departcountry,@destination,@ptc,@datecreated,@dateupdate,@city
		                                set @city_airportwithcity = null
	                                END 
                                    CLOSE @MyCursor ;
                                    DEALLOCATE @MyCursor;
                                end";
                updateData(command, connectionstring);
            }
            catch
            {
                Console.WriteLine("Error");
            }
        }
        public void deleteOldDataOfMasterPricer(pricetobeat prtobt, string connectionstring)
        {
            string stringList0 = "";
            string stringList1 = "";
            string stringList2 = "";
            string stringList_cabin0 = "";
            string stringList_cabin1 = "";
            string stringList_cabin2 = "";
            List<string> stringList = prtobt.type.Split(',').ToList();
            if (stringList.Capacity == 1) { stringList0 = stringList[0].ToString(); stringList1 = ""; stringList2 = ""; }
            if (stringList.Capacity == 2) { stringList0 = stringList[0].ToString(); stringList1 = stringList[1].ToString(); stringList2 = ""; }
            if (stringList.Capacity == 3) { stringList0 = stringList[0].ToString(); stringList1 = stringList[1].ToString(); stringList2 = stringList[2].ToString(); }
            List<string> stringList_cabin = prtobt.cabin.Split(',').ToList();
            if (stringList_cabin.Capacity == 1) { stringList_cabin0 = stringList_cabin[0].ToString(); stringList_cabin1 = ""; stringList_cabin2 = ""; }
            if (stringList_cabin.Capacity == 2) { stringList_cabin0 = stringList_cabin[0].ToString(); stringList_cabin1 = stringList_cabin[1].ToString(); stringList_cabin2 = ""; }
            if (stringList_cabin.Capacity == 3) { stringList_cabin0 = stringList_cabin[0].ToString(); stringList_cabin1 = stringList_cabin[1].ToString(); stringList_cabin2 = stringList_cabin[2].ToString(); }

            string cmddelete = @"delete dbo.Bot_MasterPricer_flightBack where Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "' and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') "
                   + "delete dbo.Bot_MasterPricer_flightGo where Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "'  and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') "
                   + "delete dbo.Bot_MasterPricer_GroupOfFares where Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "'  and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') "
                   + "delete dbo.Bot_MasterPricer_Master_Pricer where Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "'  and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') "
                   + "delete dbo.Bot_MasterPricer_recordFareMasterPricer where datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "'  and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') "
                   + "delete dbo.Bot_MasterPricer_Avg_city where Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and Type in ('" + stringList0 + "','" + stringList1 + "','" + stringList2 + "') and City='" + prtobt.city + "'  and cabin in ('" + stringList_cabin0 + "','" + stringList_cabin1 + "','" + stringList_cabin2 + "') ";
            updateData(cmddelete, connectionstring);
        }
        public List<string> splitStringwithString(string allText, char separatestr)
        {
            string[] strarr = allText.Split(separatestr);
            List<string> lststr = new List<string>();
            foreach (string item in strarr)
            {
                lststr.Add(item);
            }
            return lststr;
        }
        public string convertnull(string val)
        {
            if (val == null)
            {
                return "";
            }
            else
            {
                return val;
            }
        }
        private string convertBoolToString(bool valbool)
        {
            if (valbool)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        public string convertCabinClass(string cabclass)
        {
            if (cabclass == "12")
            {
                return "F";
            }
            else if (cabclass == "8")
            {
                return "C";
            }
            else if (cabclass == "19")
            {
                return "W";
            }
            else //"20"
            {
                return "Y";
            }
        }
        public string addSingleQuote(string fullText)
        {
            return "'" + fullText + "'";
        }
        public DateTime convertStringToDateTime(string datestring)
        {
            DateTime datetime = Convert.ToDateTime(datestring);
            string onlyyearstr = datetime.ToString("yyyy");
            string dayandmonthstr = datetime.ToString("MM-dd");

            if (Convert.ToInt32(onlyyearstr) > 2400)
            {
                onlyyearstr = (Convert.ToInt32(onlyyearstr) - 543).ToString();
            }
            string newdatetime = onlyyearstr + "-" + dayandmonthstr;
            return Convert.ToDateTime(newdatetime);
        }
        public void updateRecordFlightMP(pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass, string connectionstring)
        {
            string cmdupdate = @"
                DECLARE @MyCursor CURSOR;
                DECLARE @MyCursor_filghtnumbergo CURSOR;
                DECLARE @MyCursor_filghtnumberback CURSOR;
                DECLARE @filghtnumbergo varchar(255);
                DECLARE @listfilghtnumbergo varchar(8000)='';
                DECLARE @filghtnumberback varchar(255);
                DECLARE @listfilghtnumberback varchar(8000)='';
                DECLARE @date varchar(255);
                DECLARE @TyperefNumber_table varchar(255);
                DECLARE @TypeMasterPricer_table varchar(255);
                DECLARE @TripMaster_table varchar(255);
                DECLARE @cabin varchar(255);
                DECLARE @total varchar(255);
                DECLARE @tax varchar(255);
                DECLARE @airline varchar(255);
                DECLARE @farebasis varchar(255);
                DECLARE @rbd varchar(255);
                DECLARE @rbdgo varchar(255);
                DECLARE @rbdback varchar(255);
                DECLARE @faretype varchar(255);
                DECLARE @trip varchar(255);
                DECLARE @departcountry varchar(255);
                DECLARE @destination varchar(255);
                DECLARE @ptc varchar(255);
                declare @city_table varchar(255);
                declare @lastofticket varchar(255);
                declare @des1 varchar(255);
                declare @percent int;
                declare @avlStatus nchar(10);
                DECLARE @SearchStringairline varchar(255);
                DECLARE @SearchStringfarebasis varchar(255);
                DECLARE @SearchStringrbd varchar(255);
                DECLARE @SearchStringcabin varchar(255);
                DECLARE @SearchStringfaretype varchar(255);
                DECLARE @SearchStringtrip varchar(255);
                DECLARE @SearchStringTripMaster varchar(255);
                DECLARE @SearchStringdepartcountry varchar(255);
                DECLARE @SearchStringdestination varchar(255);
                DECLARE @SearchStringptc varchar(255);
                declare @Searchcity varchar(200);
                DECLARE @avg float;
                DECLARE @d int;
                DECLARE @rank int;
                DECLARE @insertrank int;
                DECLARE @net money;
                DECLARE @taxOfAdults varchar(255);
                DECLARE @testairline varchar(255);

                BEGIN
                    SET @MyCursor = CURSOR FOR
                    (select m.Datetime,g.TyperefNumber,m.TypeMasterPricer,m.TripMaster,m.TotalPrice,m.TaxPrice,f.marketingCarrier,g.fareBasis,g.rbd,g.cabin,g.fareType,f.locationIdGO,m.City,m.ptc, 
                        SUBSTRING(PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 1),13,7),PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 2)
                    from  dbo.Bot_MasterPricer_flightGo as f  
                   inner join dbo.Bot_MasterPricer_Master_Pricer as m 
                   on m.TripMaster=f.TripMaster and m.TypeMasterPricer=f.TypeMasterPricer and m.City=f.City and m.Datetime=f.Datetime and f.refNumberGo=m.refNumberGo and m.cabin=f.cabin
                   inner join dbo.Bot_MasterPricer_GroupOfFares as g
                   on g.itemNumber=m.itemNumber and g.City=m.City and g.TripMaster=m.TripMaster and g.TypeMasterPricer=m.TypeMasterPricer and g.Datetime=m.Datetime  and g.cabin=m.cabin 
                   where m.City='" + prtbt.city + "' and g.City='" + prtbt.city + "' and f.City='" + prtbt.city + "' and f.TripMaster in ('R','O') and m.TripMaster in ('R','O')  and g.TripMaster in ('R','O') and f.TypeMasterPricer in ('" + typemaster + "','" + typemaster + "','" + typemaster + "') "
                   + "and g.TypeMasterPricer in ('" + typemaster + "','" + typemaster + "','" + typemaster + "') and m.TypeMasterPricer in ('" + typemaster + "','" + typemaster + "','" + typemaster + "') and m.cabin in ('" + cabclass + "','" + cabclass + "','" + cabclass + "','" + cabclass + "') and m.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and f.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and g.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) "
                   + "and g.TyperefNumber in ('go') and f.locationIdBack not in ('BKK','DMK') and f.noOfAdults='" + conf.noOfAdults + "' and m.noOfAdults='" + conf.noOfAdults + "' and g.noOfAdults='" + conf.noOfAdults + "' group by m.Datetime,g.TyperefNumber,m.TypeMasterPricer,m.TripMaster,m.TotalPrice,m.TaxPrice,f.marketingCarrier,g.fareBasis,g.rbd,g.cabin,g.fareType,f.locationIdGO,m.City,m.ptc,SUBSTRING(PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 1),13,7),PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 2) ) order by m.TaxPrice "
                   + "OPEN @MyCursor FETCH NEXT FROM @MyCursor  INTO @date,@TyperefNumber_table,@TypeMasterPricer_table,@TripMaster_table,@total,@tax,@airline,@farebasis,@rbd,@cabin,@faretype,@departcountry,@city_table,@ptc,@lastofticket,@des1 WHILE @@FETCH_STATUS = 0 BEGIN "
                     + "select @SearchStringairline= (select dbo.RemoveNonAlphaCharacters(@airline)) "
                     + "select @SearchStringfarebasis= (select dbo.RemoveNonAlphaCharacters(@farebasis)) "
                     + "select @SearchStringcabin= (select dbo.RemoveNonAlphaCharacters(@cabin)) "
                     + "select @SearchStringfaretype= (select dbo.RemoveNonAlphaCharacters(@faretype)) "
                     + "select @SearchStringtrip= (select dbo.RemoveNonAlphaCharacters(@TypeMasterPricer_table)) "
                     + "select @SearchStringTripMaster= (select dbo.RemoveNonAlphaCharacters(@TripMaster_table)) "
                     + "select @SearchStringdepartcountry= (select dbo.RemoveNonAlphaCharacters(@departcountry)) "
                     + "select @SearchStringdestination= (select dbo.RemoveNonAlphaCharacters(@destination)) "
                     + "select @SearchStringptc= (select dbo.RemoveNonAlphaCharacters(@ptc)) "
                     + "select @Searchcity = (select dbo.RemoveNonAlphaCharacters(@city_table)) "
                     + "set @rbdgo=PARSENAME(REPLACE(@rbd, ',', '.'), 2) "
                     + "set @rbdback=PARSENAME(REPLACE(@rbd, ',', '.'), 1) "
                     + "set @avlStatus = (select top 1 g.avlStatus from dbo.Bot_MasterPricer_Master_Pricer as m inner join dbo.Bot_MasterPricer_flightGo as f on m.Datetime=f.Datetime and m.TripMaster=f.TripMaster "
                     + "and m.TypeMasterPricer=f.TypeMasterPricer and m.City=f.City and f.refNumberGo=m.refNumberGo and m.cabin=f.cabin inner join dbo.Bot_MasterPricer_GroupOfFares as g "
                     + "on g.itemNumber=m.itemNumber and g.City=m.City and g.TripMaster=m.TripMaster and g.TypeMasterPricer=m.TypeMasterPricer and g.Datetime=m.Datetime and g.cabin=m.cabin "
                     + "where TotalPrice=@total and TaxPrice=@tax and marketingCarrier=@SearchStringairline and fareBasis=@SearchStringfarebasis and rbd=@rbd and m.cabin=@SearchStringcabin and m.City=@Searchcity and g.fareType=@SearchStringfaretype "
                     + "and m.Datetime=@date and m.TypeMasterPricer=@SearchStringtrip order by g.avlStatus asc) "

                     //--FilghtnumberGO
                     + " SET @MyCursor_filghtnumbergo = CURSOR FOR select f.flightNumber "
                     + "from  dbo.Bot_MasterPricer_flightGo as f  "
                     + "inner join dbo.Bot_MasterPricer_Master_Pricer as m "
                     + "on m.TripMaster=f.TripMaster and m.TypeMasterPricer=f.TypeMasterPricer and m.City=f.City and m.Datetime=f.Datetime and f.refNumberGo=m.refNumberGo and m.cabin=f.cabin "
                     + "inner join dbo.Bot_MasterPricer_GroupOfFares as g "
                     + "on g.itemNumber=m.itemNumber and g.City=m.City and g.TripMaster=m.TripMaster and g.TypeMasterPricer=m.TypeMasterPricer and g.Datetime=m.Datetime  and g.cabin=m.cabin  "
                     + "where m.City=@Searchcity  and m.TripMaster =@SearchStringTripMaster and m.TypeMasterPricer =@SearchStringtrip "
                     + "and f.locationIdBack not in ('BKK','DMK') and g.TyperefNumber in ('GO') and m.Datetime=@date  and g.fareBasis=@SearchStringfarebasis "
                     + "and g.rbd like '%'+@rbdgo+'%' and g.cabin=@SearchStringcabin and g.fareType=@SearchStringfaretype and m.TotalPrice=@total and m.TaxPrice=@tax  and m.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and f.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and g.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) "
                     + "and f.noOfAdults='" + conf.noOfAdults + "' and m.noOfAdults='" + conf.noOfAdults + "' and g.noOfAdults='" + conf.noOfAdults + "' "
                     + "group by m.Datetime,g.TyperefNumber,m.TypeMasterPricer,m.TripMaster,m.TotalPrice,m.TaxPrice,f.marketingCarrier,g.fareBasis,g.rbd,g.cabin,g.fareType,f.locationIdGO,m.City,m.ptc,f.flightNumber "
                     + ",SUBSTRING(PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 1),13,7),PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 2)  order by m.TaxPrice "
                     + "OPEN @MyCursor_filghtnumbergo  "
                     + "FETCH NEXT FROM @MyCursor_filghtnumbergo INTO @filghtnumbergo "
                     + "WHILE @@FETCH_STATUS = 0 "
                     + "BEGIN "
                     + "select @filghtnumbergo = (select dbo.RemoveNonAlphaCharacters(@filghtnumbergo)) "
                     + "set @listfilghtnumbergo += @filghtnumbergo+',' "
                     + "FETCH NEXT FROM @MyCursor_filghtnumbergo INTO @filghtnumbergo "
                     + "END  "
                     + "CLOSE @MyCursor_filghtnumbergo "
                     + "DEALLOCATE @MyCursor_filghtnumbergo "

                     //--FilghtnumberBACK
                     + "SET @MyCursor_filghtnumberback = CURSOR FOR select f.flightNumber "
                     + "from  dbo.Bot_MasterPricer_flightBack as f   "
                     + "inner join dbo.Bot_MasterPricer_Master_Pricer as m "
                     + "on m.TripMaster=f.TripMaster and m.TypeMasterPricer=f.TypeMasterPricer and m.City=f.City and m.Datetime=f.Datetime and f.refNumberBack=m.refNumberBack and m.cabin=f.cabin "
                     + "inner join dbo.Bot_MasterPricer_GroupOfFares as g "
                     + "on g.itemNumber=m.itemNumber and g.City=m.City and g.TripMaster=m.TripMaster and g.TypeMasterPricer=m.TypeMasterPricer and g.Datetime=m.Datetime  and g.cabin=m.cabin "
                     + "where m.City=@Searchcity  and m.TripMaster =@SearchStringTripMaster and m.TypeMasterPricer =@SearchStringtrip "
                     + "and g.TyperefNumber in ('GO') and m.Datetime=@date  and g.fareBasis=@SearchStringfarebasis "
                     + "and g.rbd like '%'+@rbdback+'%' and g.cabin=@SearchStringcabin and g.fareType=@SearchStringfaretype and m.TotalPrice=@total and m.TaxPrice=@tax  and m.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and f.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) and g.Datetime=CONVERT(VARCHAR(10),GETDATE(),120) "
                     + "and f.noOfAdults='" + conf.noOfAdults + "' and m.noOfAdults='" + conf.noOfAdults + "' and g.noOfAdults='" + conf.noOfAdults + "' "
                     + "group by m.Datetime,g.TyperefNumber,m.TypeMasterPricer,m.TripMaster,m.TotalPrice,m.TaxPrice,f.marketingCarrier,g.fareBasis,g.rbd,g.cabin,g.fareType,f.locationIdBack,m.City,m.ptc,f.flightNumber "
                     + ",SUBSTRING(PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 1),13,7),PARSENAME(REPLACE(CAST(m.description AS VARCHAR(255)), ',', '.'), 2)  order by m.TaxPrice "
                     + "OPEN @MyCursor_filghtnumberback  "
                     + "FETCH NEXT FROM @MyCursor_filghtnumberback INTO @filghtnumberback "
                     + "WHILE @@FETCH_STATUS = 0 "
                     + "BEGIN "
                     + "select @filghtnumberback = (select dbo.RemoveNonAlphaCharacters(@filghtnumberback)) "
                     + "set @listfilghtnumberback += @filghtnumberback+',' "
                     + "FETCH NEXT FROM @MyCursor_filghtnumberback INTO @filghtnumberback "
                     + "END  "
                     + "CLOSE @MyCursor_filghtnumberback "
                     + "DEALLOCATE @MyCursor_filghtnumberback "

                     + "select @d = Discount from dbo.Bot_MasterPricer_tax where (Airline =@SearchStringairline or Airline='*') and (FareBasis =@SearchStringfarebasis or FareBasis='*')  "
                     + "and (RBD like '%'+@rbdgo+'%' or RBD='*') and (FareType =@SearchStringfaretype or FareType ='*') and (Trip like '%'+@SearchStringtrip+'%' or Trip='*')  "
                     + "and (DepartCountry like '%'+@SearchStringdepartcountry+'%' or DepartCountry='*') and (city like '%'+@Searchcity+'%' or city='*') and (Ptc like '%'+@SearchStringptc+'%' or Ptc='*') "
                     + "order by city,Destination,FareBasis,RBD,Airline,Ptc,DepartCountry,FareType "

                     + "if('" + conf.noOfAdults + "'='2') "
                     + "begin "
                     + "select @net = ((@total/2)-(@tax/2)) - (@d*0.01*((@total/2)-(@tax/2))) "
                     + "select @taxOfAdults = @tax/2 "
                     + "end "
                     + "if('" + conf.noOfAdults + "'='1') "
                     + "begin "
                     + "select @net = ((@total/1)-(@tax/1)) - (@d*0.01*((@total/1)-(@tax/1))) "
                     + "select @taxOfAdults = @tax "
                     + "end "
                     + "set @net =  5 * FLOOR(( CEILING(@net)+4)/5) "
                     + "insert into dbo.Bot_MasterPricer_recordFareMasterPricer (datetime,TyperefNumber,TypeMasterPricer,TripMaster,TotalPrice,TaxPrice,marketingCarrier,fareBasis,rbd,cabin,fareType,locationIdGO,city,ptc,lastofticket,des1,FilghtNumberGO,FilghtNumberBACK,avlStatus,noOfAdults) "
                     + "values (@date,@TyperefNumber_table,@TypeMasterPricer_table,@TripMaster_table,@net,@taxOfAdults,@airline,@farebasis,@rbd,@cabin,@faretype,@departcountry,@city_table,@ptc,@lastofticket,@des1,@listfilghtnumbergo,@listfilghtnumberback,@avlStatus,'" + conf.noOfAdults + "') "
                     + "set @listfilghtnumbergo ='' "
                     + "set @listfilghtnumberback ='' "
                     + "FETCH NEXT FROM @MyCursor "
                     + "INTO @date,@TyperefNumber_table,@TypeMasterPricer_table,@TripMaster_table,@total,@tax,@airline,@farebasis,@rbd,@cabin,@faretype,@departcountry,@city_table,@ptc,@lastofticket,@des1 "
                     + "END "

                     + "CLOSE @MyCursor ; "
                     + "DEALLOCATE @MyCursor; "
                     + "END;";
            updateData(cmdupdate, connectionstring);
        }
        public void updateAvgcity(pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass, string connectionstring)
        {
            string cmdupdate = @"   insert into dbo.Bot_MasterPricer_Avg_city (City,Avg,Datetime,Type,cabin) values ('" + prtbt.city + "',(select avg((q1.TotalPrice)+(q1.TaxPrice)) from( select "
                + "row_number() over (partition by cabin,PARSENAME(REPLACE(rbd, ',', '.'), 2),marketingCarrier order by TotalPrice )  as rowno,* "
                + "from dbo.Bot_MasterPricer_recordFareMasterPricer where city='" + prtbt.city + "' and cabin='" + cabclass + "' and TypeMasterPricer='" + typemaster + "' and datetime=CONVERT(VARCHAR(10),GETDATE(),120) and (TotalPrice+TaxPrice)<(select avg((q.TotalPrice)+(q.TaxPrice))*2 from( select "
                + "row_number() over (partition by cabin,PARSENAME(REPLACE(rbd, ',', '.'), 2),marketingCarrier order by TotalPrice )  as rowno,* "
                + "from dbo.Bot_MasterPricer_recordFareMasterPricer where city='" + prtbt.city + "' and cabin='" + cabclass + "' and TypeMasterPricer='" + typemaster + "' and datetime=CONVERT(VARCHAR(10),GETDATE(),120)) q  "
                + "where rowno <= 2 )) q1 "
                + "where rowno <= 2),CONVERT(VARCHAR(10),GETDATE(),110),'" + typemaster + "','" + cabclass + "') "

                + "update dbo.Bot_MasterPricer_recordFareMasterPricer set percents=((dbo.Bot_MasterPricer_Avg_city.Avg-((Bot_MasterPricer_recordFareMasterPricer.TotalPrice)+(Bot_MasterPricer_recordFareMasterPricer.taxprice)))/cast(abs(dbo.Bot_MasterPricer_Avg_city.Avg) as float))*100  "
                + "from dbo.Bot_MasterPricer_recordFareMasterPricer inner join dbo.Bot_MasterPricer_Avg_city on Bot_MasterPricer_recordFareMasterPricer.city=Bot_MasterPricer_Avg_city.City and Bot_MasterPricer_recordFareMasterPricer.TypeMasterPricer=Bot_MasterPricer_Avg_city.Type  "
                + "and Bot_MasterPricer_recordFareMasterPricer.cabin=Bot_MasterPricer_Avg_city.cabin and Bot_MasterPricer_recordFareMasterPricer.datetime=Bot_MasterPricer_Avg_city.Datetime ";
            updateData(cmdupdate, connectionstring);
        }
        public void updateRecordFareMP(pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass, string connectionstring)
        {

            string cmdupdate = @"
                    DECLARE @MyCursor CURSOR;
                    DECLARE @datetime varchar(255);
                    DECLARE @typerefnumber varchar(255);
                    DECLARE @typemasterpricer varchar(255);
                    DECLARE @tripmaster varchar(255);
                    DECLARE @refnumber varchar(255);
                    DECLARE @totalprice varchar(255);
                    DECLARE @taxprice varchar(255);
                    DECLARE @marketingcarrier varchar(255);
                    DECLARE @farebasis varchar(255);
                    DECLARE @rbd varchar(255);
                    declare @cabin varchar(255);
                    DECLARE @faretype varchar(255);
                    DECLARE @locationidgo varchar(255);
                    DECLARE @city varchar(255);
                    DECLARE @ptc varchar(255);
                    DECLARE @percents int;
                    declare @rank int;
                    declare @lastofticket nvarchar(max);
                    declare @des1 nvarchar(max);

                    DECLARE @rank1 int;
                    DECLARE @rank2 int;
                    DECLARE @rank3 int;
                    DECLARE @rank4 int;
                    DECLARE @rank5 int;
                    DECLARE @rank6 int;
                    DECLARE @insertrank int;
                        select @rank1 = rank1 from dbo.Bot_MasterPricer_Rank
                        select @rank2 = rank2 from dbo.Bot_MasterPricer_Rank
                        select @rank3 = rank3 from dbo.Bot_MasterPricer_Rank
                        select @rank4 = rank4 from dbo.Bot_MasterPricer_Rank
                        select @rank5 = rank5 from dbo.Bot_MasterPricer_Rank
                        select @rank6 = rank6 from dbo.Bot_MasterPricer_Rank
                    BEGIN
                     SET @MyCursor = CURSOR FOR select datetime,TyperefNumber,TypeMasterPricer,TripMaster,TotalPrice,TaxPrice,marketingCarrier,fareBasis,rbd,cabin,fareType,locationIdGO,city,ptc,percents,rank,lastofticket,des1 from dbo.Bot_MasterPricer_recordFareMasterPricer where datetime=CONVERT(VARCHAR(10),GETDATE(),120) and TypeMasterPricer='" + typemaster + "' and cabin='" + cabclass + "' and city='" + prtbt.city + "'"
                     + @"OPEN @MyCursor FETCH NEXT FROM @MyCursor INTO @datetime,@typerefnumber,@typemasterpricer,@tripmaster,@totalprice,@taxprice,@marketingcarrier,@farebasis,@rbd,@cabin,@faretype,@locationidgo,@city,@ptc,@percents,@rank,@lastofticket,@des1
                        WHILE @@FETCH_STATUS = 0
                        BEGIN
                            if(@percents>=@rank1)
                            begin
                                select @insertrank = 1
                            end
                            else if(@percents>=@rank2 and @percents < @rank1)
                            begin
                                select @insertrank = 2
                            end
                            else if(@percents>=@rank3 and @percents < @rank2)
                            begin
                                select @insertrank = 3
                            end
                            else if(@percents>=@rank4 and @percents<@rank3)
                            begin
                                select @insertrank = 4
                            end
                            else if(@percents >= @rank5 and @percents < @rank4)
                            begin
                                select @insertrank = 5
                            end
                            else if(@percents >= @rank6 and @percents < @rank5)
                            begin
                                select @insertrank = 6
                            end
                            else if(@percents<@rank6)
                            begin
                                select @insertrank = 7
                            end
                            update dbo.Bot_MasterPricer_recordFareMasterPricer set rank = @insertrank where datetime=@datetime and TyperefNumber=@typerefnumber and TypeMasterPricer=@typemasterpricer
                            and TripMaster=@tripmaster and TotalPrice=@totalprice and TaxPrice=@taxprice and marketingCarrier=@marketingcarrier
                            and fareBasis=@farebasis and rbd=@rbd and cabin=@cabin and fareType=@faretype and locationIdGO=@locationidgo and city=@city
                            and ptc =@ptc and percents=@percents and lastofticket=@lastofticket and (des1=@des1 or des1 is null)
                            --print @insertrank
                              FETCH NEXT FROM @MyCursor INTO @datetime,@typerefnumber,@typemasterpricer,@tripmaster,@totalprice,@taxprice,@marketingcarrier,@farebasis,@rbd,@cabin,@faretype,@locationidgo,@city,@ptc,@percents,@rank,@lastofticket,@des1
                        END 
                        CLOSE @MyCursor 
                        DEALLOCATE @MyCursor
                    END";
            updateData(cmdupdate, connectionstring);
        }
        public void updateSpecialFareOK(pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass, string connectionstring)
        {
            SqlConnection connection = new SqlConnection(connectionstring);
            SqlCommand cmd5 = new SqlCommand(@"
                                            DECLARE @MyCursor CURSOR;
                                            DECLARE @datetime varchar(255);
                                            DECLARE @TyperefNumber varchar(255);
                                            DECLARE @TypeMasterPricer varchar(255);
                                            DECLARE @TripMaster varchar(255);
                                            DECLARE @TotalPrice int;
                                            DECLARE @TaxPrice int;
                                            DECLARE @Airline varchar(255);
                                            DECLARE @FareBasis varchar(255);
                                            DECLARE @rbd varchar(255);
                                            DECLARE @Fullrbd varchar(255);
                                            DECLARE @cabin nvarchar(255);
                                            DECLARE @faretype varchar(255);
                                            DECLARE @locationGo varchar(255);
                                            DECLARE @locationBack varchar(255);
                                            declare @city varchar(200);
                                            DECLARE @ptc varchar(255);
                                            DECLARE @percents varchar(255);
                                            DECLARE @rank varchar(255);
                                            declare @lastofticket varchar(255);
                                            declare @des1 varchar(255);
                                            declare @FilghtNumberGO varchar(255);
                                            declare @FilghtNumberBACK varchar(255);
                                            declare @avlStatus nchar(10);
                                            DECLARE @SpecialFareOID_table nvarchar(255);
                                            declare @netfare int;
                                            declare @taxfare int;
                                            DECLARE @total int;
                                            DECLARE @tax int;
                                            declare @ID nvarchar(max);
                                            declare @m nvarchar(10);
                                            declare @d nvarchar(10);
                                            declare @y nvarchar(10);
                                            declare @convert_m nvarchar(10);
                                            declare @FareCityImageID uniqueidentifier;
                                            declare @ImageUrl nvarchar(500);
                                            declare @ailId nvarchar(10);
                                            declare @FlightType nvarchar(1);
                                            declare @insert int = 0;
                                            declare @update int = 0;
                                            declare @bookingend int = 0;
                                            declare @HotFare bit = 0;
                                            declare @SpecialFareName nvarchar(100);
                                            DECLARE @list_SpecialFareName_Hotfarebookingend nvarchar(4000) = '' ;
                                            DECLARE @list_SpecialFareName_Horfarepriceupdate nvarchar(4000) = '' ;
                                            DECLARE @list_SpecialFareName_Hotfarebookingend_ids nvarchar(4000) = '' ;
                                            DECLARE @list_SpecialFareName_Horfarepriceupdate_ids nvarchar(4000) = '' ;
                                            DECLARE @ids_log int;

                                            DECLARE @SpecialFareOIDlist_insert nvarchar(4000) = '' ;
                                            DECLARE @SpecialFareOIDlist_update nvarchar(4000) = '' ;
                                            DECLARE @SpecialFareOIDlist_bookingend nvarchar(4000) = '' ;
                                            BEGIN

	
	                                            SET @MyCursor = CURSOR FOR select datetime,TyperefNumber,TypeMasterPricer,TripMaster,TotalPrice,TaxPrice,marketingCarrier,fareBasis,rbd,cabin,fareType,locationIdGO,locationIdBack,city,ptc,percents,rank,
	                                            lastofticket,des1,FilghtNumberGO,FilghtNumberBACK,avlStatus from (select ROW_NUMBER() OVER (partition by datetime,TyperefNumber,TypeMasterPricer,TripMaster,marketingCarrier,PARSENAME(REPLACE(rbd, ',', '.'), 2) ,cabin,fareType,locationIdGO,city order by TotalPrice+TaxPrice asc) AS id ,*
	                                            from dbo.Bot_MasterPricer_recordFareMasterPricer where datetime=CONVERT(VARCHAR(10),GETDATE(),120) and city=@city1 and TypeMasterPricer=@TypeMasterPricer1 and cabin=@cabin1 ) q where id <2  order by marketingCarrier,fareBasis,rbd,cabin asc
	                                            OPEN @MyCursor 
	                                            FETCH NEXT FROM @MyCursor INTO @datetime,@TyperefNumber,@TypeMasterPricer,@TripMaster,@TotalPrice,@TaxPrice,@Airline,@FareBasis,@rbd,@cabin,@faretype,@locationGo,@locationBack,@city,@ptc,@percents,@rank,@lastofticket,@des1,@FilghtNumberGO,@FilghtNumberBACK,@avlStatus
                                                while @@FETCH_STATUS = 0
	                                            BEGIN 
		                                            set @tax = @TaxPrice
		                                            set @total = @TotalPrice
		                                            set @d = SUBSTRING(@lastofticket,1,2)
		                                            set @m = SUBSTRING(@lastofticket,3,3)
		                                            set @y = SUBSTRING(@lastofticket,6,2)
		                                            if(@m='JAN') set @convert_m='/01/'
		                                            if(@m='FEB') set @convert_m='/02/'
		                                            if(@m='MAR') set @convert_m='/03/'
		                                            if(@m='APR') set @convert_m='/04/'
		                                            if(@m='MAY') set @convert_m='/05/'
		                                            if(@m='JUN') set @convert_m='/06/'
		                                            if(@m='JUL') set @convert_m='/07/'
		                                            if(@m='AUG') set @convert_m='/08/'
		                                            if(@m='SEP') set @convert_m='/09/'
		                                            if(@m='OCT') set @convert_m='/10/'
		                                            if(@m='NOV') set @convert_m='/11/'
		                                            if(@m='DEC') set @convert_m='/12/'
        
                                                    select @Fullrbd = PARSENAME(REPLACE(@rbd, ',', '.'), 2)+','+(select dbo.RemoveNonAlphaCharacters(PARSENAME(REPLACE(@rbd, ',', '.'), 1)))
		                                            select @TyperefNumber= (select dbo.RemoveNonAlphaCharacters(@TyperefNumber))
		                                            select @TypeMasterPricer= (select dbo.RemoveNonAlphaCharacters(@TypeMasterPricer))
		                                            select @TripMaster= (select dbo.RemoveNonAlphaCharacters(@TripMaster))
		                                            select @Airline= (select dbo.RemoveNonAlphaCharacters(@Airline))
		                                            select @FareBasis= (select dbo.RemoveNonAlphaCharacters(@FareBasis))
		                                            set @rbd=PARSENAME(REPLACE(@rbd, ',', '.'), 2) 
		                                            select @cabin= (select dbo.RemoveNonAlphaCharacters(@cabin))
		                                            select @faretype= (select dbo.RemoveNonAlphaCharacters(@faretype))
		                                            select @locationGo= (select dbo.RemoveNonAlphaCharacters(@locationGo))
		                                            select @locationBack= (select dbo.RemoveNonAlphaCharacters(@locationBack))
		                                            select @city = (select dbo.RemoveNonAlphaCharacters(@city))
		                                            select @ptc= (select dbo.RemoveNonAlphaCharacters(@ptc))
		                                            select @percents= (select dbo.RemoveNonAlphaCharacters(@percents))
		                                            select @rank= (select dbo.RemoveNonAlphaCharacters(@rank))
		                                            set @ailId = (select ailId from dbo.AirlineWithImage where AirlineCode=@Airline)
		                                            set @id = NEWID() 

                                                    if(@TypeMasterPricer = '23')
		                                            begin
			                                            set @FlightType='D'
		                                            end
		                                            if(@TypeMasterPricer = '24' or @TypeMasterPricer='26')
		                                            begin
			                                            set @FlightType='T'
		                                            end


		                                            UPDATE dbo.SpecialFare_FareOK set [FacebookPostID]=[FacebookPostID]  WHERE ((TripType=@TripMaster) and (OriginalCity=@locationGo) 
		                                            and (DestinationCity=@city) and ( (Airline=@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class=@rbd) and (FareBasis=@FareBasis)
		                                            and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1' )
		                                            if @@ROWCOUNT >0
		                                            begin
                                                            select @HotFare = HotFare from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster ) and (OriginalCity =@locationGo) 
		                                            and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)
		                                            and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1'  )
                                                            if(@HotFare=0) 
		                                                    begin
			                                                    select @SpecialFareOID_table = SpecialFareOID from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster ) and (OriginalCity =@locationGo) 
		                                                        and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)
		                                                        and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1'  )
				                                                select @netfare = NetFareADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                select @taxfare = FareTaxADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                update dbo.Bot_MasterPricer_LogQueryFareOK set date=GETDATE(),TypeQuery='BookingEnd',NetFare=@netfare,Tax=@taxfare,remark='จอง : วันนี้เป็นต้นไป เดินทาง : วันนี้เป็นต้นไป' where SpecialFareOID=@SpecialFareOID_table
				                                                update dbo.SpecialFare_FareOK set TripType=@TripMaster,OriginalCity=@locationGo,DestinationCity=@city
				                                                ,BookingEnd=CONVERT(datetime, '20'+@y+@convert_m+@d+' 00:00:00', 120),Remark='จอง : วันนี้เป็นต้นไป เดินทาง : วันนี้เป็นต้นไป',Remark2=@des1,Airline=@Airline,DefaultAdult='1'
				                                                ,FareTagStart=@cabin+','+@TypeMasterPricer,FareTaxADT=@tax,Class=@rbd,FareBasis=@FareBasis,NetFareADT=@total,RANK=@rank,avlStatus=@avlStatus,ailId=@ailId,TotalADT=(@total+@tax),SpecialFareName=''+@locationGo+'-'+@city+','+@Airline+',('+@Fullrbd+'),'+@FareBasis+','+@datetime+'(BOT)'+'',FlightType=@FlightType where SpecialFareOID=@SpecialFareOID_table
				                                                update dbo.Bot_MasterPricer_refFlightNumber set FlightNumberGO=@FilghtNumberGO,FlightNumberBACK=@FilghtNumberBACK where SpecialFareOID=@SpecialFareOID_table
                                                                set @bookingend = @bookingend + 1 ;
                                                                set @SpecialFareOIDlist_bookingend = @SpecialFareOIDlist_bookingend + ',' + @SpecialFareOID_table ;
		                                                    end
                                                            else
		                                                    begin
			                                                    select @SpecialFareName = SpecialFareName from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster ) and (OriginalCity =@locationGo) 
		                                                            and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)
		                                                            and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1'  )
                                                                select @ids_log = IDS from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster ) and (OriginalCity =@locationGo) 
		                                                            and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)
		                                                            and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1'  )
                                                                select @SpecialFareOID_table = SpecialFareOID from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster ) and (OriginalCity =@locationGo) 
		                                                            and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)
		                                                            and (BookingEnd < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))  ) and DefaultAdult='1'  )
				                                                select @netfare = NetFareADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                select @taxfare = FareTaxADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
                                                                set @list_SpecialFareName_Hotfarebookingend_ids = @list_SpecialFareName_Hotfarebookingend_ids + ',' +  CAST(@ids_log as nvarchar);
                                                                set @list_SpecialFareName_Hotfarebookingend = @list_SpecialFareName_Hotfarebookingend + '&' + @SpecialFareName + '  ราคาเก่า(Net=' + CAST(@netfare as nvarchar) + ',Tax='+CAST(@taxfare as nvarchar)+')  ราคาใหม่(Net='+CAST(@total as nvarchar)+',Tax='+CAST(@tax as nvarchar)+')' ;
		                                                    end
             
		                                            end

		                                            IF @@ROWCOUNT=0
		                                            begin
			                                            if exists (select * from dbo.SpecialFare_FareOK WHERE ( (TripType=@TripMaster) and (OriginalCity=@locationGo) 
		                                            and (DestinationCity=@city) and ( (Airline=@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class=@rbd) and (FareBasis=@FareBasis) 
		                                            and (DefaultAdult='1') and ( (NetFareADT!=@total) or (FareTaxADT!=@tax) ) ) )
			                                            begin
                                                            select @HotFare = HotFare from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster) and (OriginalCity =@locationGo) 
		                                                    and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)and DefaultAdult='1' ) 
				                                                if(@HotFare=0)
		                                                        begin
                                                                    select @SpecialFareOID_table = SpecialFareOID from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster) and (OriginalCity =@locationGo) 
		                                                            and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)and DefaultAdult='1' ) 
				                                                    select @netfare = NetFareADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                    select @taxfare = FareTaxADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                    update dbo.Bot_MasterPricer_LogQueryFareOK set date=GETDATE(),TypeQuery='update',NetFare=@netfare,Tax=@taxfare,remark='จอง : วันนี้เป็นต้นไป เดินทาง : วันนี้เป็นต้นไป' where SpecialFareOID=@SpecialFareOID_table
				                                                    update dbo.SpecialFare_FareOK set NetFareADT=@total,FareTaxADT=@tax,BookingEnd=CONVERT(datetime, '20'+@y+@convert_m+@d+' 00:00:00', 120),RANK=@rank,avlStatus=@avlStatus,ailId=@ailId,TotalADT=(@total+@tax),SpecialFareName=''+@locationGo+'-'+@city+','+@Airline+',('+@Fullrbd+'),'+@FareBasis+','+@datetime+'(BOT)'+'',FlightType=@FlightType where  SpecialFareOID=@SpecialFareOID_table
				                                                    update dbo.Bot_MasterPricer_refFlightNumber set FlightNumberGO=@FilghtNumberGO,FlightNumberBACK=@FilghtNumberBACK where SpecialFareOID=@SpecialFareOID_table
                                                                    set @update = @update + 1 ;
                                                                    set @SpecialFareOIDlist_update = @SpecialFareOIDlist_update + ',' + @SpecialFareOID_table ;
                                                                end
                                                                else
		                                                        begin
                                                                    select @SpecialFareName = SpecialFareName from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster) and (OriginalCity =@locationGo) 
		                                                                and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)and DefaultAdult='1' ) 
                                                                    select @ids_log = IDS from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster) and (OriginalCity =@locationGo) 
		                                                                and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)and DefaultAdult='1' ) 
                                                                    select @SpecialFareOID_table = SpecialFareOID from dbo.SpecialFare_FareOK WHERE ((TripType =@TripMaster) and (OriginalCity =@locationGo) 
		                                                                and (DestinationCity =@city) and ( (Airline =@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class =@rbd) and (FareBasis =@FareBasis)and DefaultAdult='1' ) 
				                                                    select @netfare = NetFareADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
				                                                    select @taxfare = FareTaxADT from dbo.SpecialFare_FareOK where SpecialFareOID=@SpecialFareOID_table
                                                                    set @list_SpecialFareName_Horfarepriceupdate_ids = @list_SpecialFareName_Horfarepriceupdate_ids +',' + CAST(@ids_log as nvarchar);
                                                                    set @list_SpecialFareName_Horfarepriceupdate = @list_SpecialFareName_Horfarepriceupdate + '&' + @SpecialFareName + '  ราคาเก่า(Net=' + CAST(@netfare as nvarchar) + ',Tax='+CAST(@taxfare as nvarchar)+')  ราคาใหม่(Net='+CAST(@total as nvarchar)+',Tax='+CAST(@tax as nvarchar)+')' ;
                                                                end
			                                            end
			                                            if not exists (select * from dbo.SpecialFare_FareOK WHERE ( (TripType=@TripMaster) and (OriginalCity=@locationGo) 
		                                            and (DestinationCity=@city) and ( (Airline=@Airline) or (ailId=@ailId) ) and (FareTagStart like '%'+@cabin+'%') and (FareTagStart like '%'+@TypeMasterPricer+'%') and (Class=@rbd) and (FareBasis=@FareBasis) 
		                                            and (DefaultAdult='1')  ) )
			                                            begin
				                                            set @FareCityImageID=(select TOP 1 FareCityImageID from dbo.SpecialFare_FareCity as s1 inner join dbo.SpecialFare_FareCityImage as s2 on s1.FareCityID=s2.FareCityID where s1.FareCityName=@city ORDER BY NEWID())
				                                            set @ImageUrl = (select ImageUrl from dbo.[SpecialFare_FareCityImage] where FareCityImageID=@FareCityImageID)
				                                            insert into dbo.SpecialFare_FareOK (SpecialFareOID,FareCityImageID,TripType,OriginalCity,DestinationCity,BookingStart,BookingEnd,Remark,ImageUrl,Remark2,Airline,CreateDate,DefaultAdult,FareTagStart,FareTaxADT,Class,FareBasis,SpecialFareName,NetFareADT,RANK,avlStatus,ailId,TotalADT,FlightType,Orders) 
				                                            values (@ID,@FareCityImageID,@TripMaster,@locationGo,@city,CONVERT(VARCHAR(10),GETDATE(),110),CONVERT(datetime, '20'+@y+@convert_m+@d+' 00:00:00', 120),'จอง : วันนี้เป็นต้นไป เดินทาง : วันนี้เป็นต้นไป',@ImageUrl,@des1,@Airline,GETDATE(),'1',@cabin+','+@TypeMasterPricer,@tax,@rbd,@FareBasis,''+@locationGo+'-'+@city+','+@Airline+',('+@Fullrbd+'),'+@FareBasis+','+@datetime+'(BOT)'+'',@total,@rank,@avlStatus,@ailId,@total+@tax,@FlightType,0)
				                                            insert into dbo.Bot_MasterPricer_LogQueryFareOK values (GETDATE(),'insert',@ID,@total,@tax,'จอง : วันนี้เป็นต้นไป เดินทาง : วันนี้เป็นต้นไป')
				                                            insert into dbo.Bot_MasterPricer_refFlightNumber values (@ID,@ID,@FilghtNumberGO,@FilghtNumberBACK)
                                                            set @insert = @insert + 1 ;
                                                            set @SpecialFareOIDlist_insert = @SpecialFareOIDlist_insert + ',' + @ID ;
			                                            end
		                                            end
		                                            FETCH NEXT FROM @MyCursor INTO @datetime,@TyperefNumber,@TypeMasterPricer,@TripMaster,@TotalPrice,@TaxPrice,@Airline,@FareBasis,@rbd,@cabin,@faretype,@locationGo,@locationBack,@city,@ptc,@percents,@rank,@lastofticket,@des1,@FilghtNumberGO,@FilghtNumberBACK,@avlStatus
                                                end
                                                update dbo.SpecialFare_FareOK set RANK='1' where TotalADT= (select min(totaladt) from dbo.SpecialFare_FareOK where DestinationCity=@city1 and FareTagStart=@cabin1+','+@TypeMasterPricer1 and convert(varchar(10),CreateDate,120)=convert(varchar(10),getdate(),120) and SpecialFareName like '%BOT%' )
                                                select @update as 'count_update',@insert as 'count_insert',@bookingend as 'count_bookingend',@SpecialFareOIDlist_insert as 'list_insert',@SpecialFareOIDlist_update as 'list_update',@SpecialFareOIDlist_bookingend as 'list_bookingend',@list_SpecialFareName_Hotfarebookingend as 'list_SpecialFareName_Hotfarebookingend',@list_SpecialFareName_Horfarepriceupdate as 'list_SpecialFareName_Horfarepriceupdate',@list_SpecialFareName_Hotfarebookingend_ids as 'list_SpecialFareName_Hotfarebookingend_ids',@list_SpecialFareName_Horfarepriceupdate_ids as 'list_SpecialFareName_Horfarepriceupdate_ids'
                                                set @update = 0;
                                                set @insert = 0;
                                                set @bookingend = 0;
                                                set @SpecialFareOIDlist_insert = 0;
                                                set @SpecialFareOIDlist_update = 0;
                                                set @SpecialFareOIDlist_bookingend = 0;
                                                set @list_SpecialFareName_Hotfarebookingend = 0;
                                                set @list_SpecialFareName_Horfarepriceupdate = 0;
                                                set @list_SpecialFareName_Hotfarebookingend_ids = 0;
                                                set @list_SpecialFareName_Horfarepriceupdate_ids = 0;
	                                            CLOSE @MyCursor
	                                            DEALLOCATE @MyCursor
                                            END
                                            ");
            cmd5.Parameters.AddWithValue("@cabin1", cabclass);
            cmd5.Parameters.AddWithValue("@city1", prtbt.city);
            cmd5.Parameters.AddWithValue("@TypeMasterPricer1", typemaster);

            cmd5.CommandType = CommandType.Text;
            cmd5.Connection = connection;
            connection.Open();
            SqlDataReader reader_count_insert_update = cmd5.ExecuteReader();
            reader_count_insert_update.Close();
            connection.Close();
        }
        public XmlDocument callMasterPricerwithAPI(int numberofadults, string pricetobeat, string destcity, string datefromdp, string datetodp, string cabinclass, bool isnonstop, bool isdirect, bool isconnect)
        {

            var req = "{\"noOfInfants\":0,\"" +
                "noOfRecommendation\":200,\"" +
                "directFlight\":" + convertBoolToString(isdirect) + ",\"" +
                "useBookingOfficeID\":false,\"" +
                "sellingCity\":\"BKK\",\"" +
                "noOfChildren\":0,\"" +
                "cabinClass\":\"" + convertCabinClass(cabinclass) + "\",\"" +
                "flexibleDate\":0,\"" +
                "noOfAdults\":" + numberofadults + ",\"" +
                "isPriceTypeRP\":true,\"" +
                "fareBasis\":\"\",\"" +
                "session\":\"\",\"" +
                "connectFlight\":" + convertBoolToString(isconnect) + ",\"" +
                "isPriceTypeRU\":true,\"" +
                "currencyCode\":\"THB\",\"" +
                "priceToBeat\":" + Convert.ToInt32(pricetobeat) * numberofadults + ",\"" +
                "flights\":[{\"" +
                    "arrivalCity\":\"" + destcity + "\",\"" +
                    "RBDCode\":\"\",\"" +
                    "companyCode\":\"\",\"" +
                    "arrivalDateTime\":\"0001-01-01T00:00:00\",\"" +
                    "flightNumber\":\"\",\"" +
                    "departureCity\":\"BKK\",\"" +
                    "departureDateTime\":\"" + convertStringToDateTime(datefromdp) + "\"}," +

                    "{\"arrivalCity\":\"BKK\",\"" +
                    "RBDCode\":\"\",\"" +
                    "companyCode\":\"\",\"" +
                    "arrivalDateTime\":\"0001-01-01T00:00:00\",\"" +
                    "flightNumber\":\"\",\"" +
                    "departureCity\":\"" + destcity + "\",\"" +
                    "departureDateTime\":\"" + convertStringToDateTime(datetodp) + "\"}],\"" +

                "corporateContractNumber\":\"\",\"" +
                "useInterlineAgreement\":true,\"" +
                "ClientIP\":\"\",\"" +
                "ticketingCity\":\"BKK\",\"" +
                "nonstopFlight\":" + convertBoolToString(isnonstop) + ",\"" +
                "bookingOID\":\"BKKIW38EH\"" +
                "}";
            var client = new RestClient("http://penguinappws.azurewebsites.net/API/FareMasterPricerTravelBoard.aspx");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "4368250d-a7ef-08a3-aa74-af1b7946d8f0");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", req, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (!response.Content.Contains("ErrorCode"))
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(response.Content);
                string content = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"><Header></Header><Body>" + doc.InnerXml + "</Body></Envelope>";
                XmlDocument newdoc = new XmlDocument();
                newdoc.LoadXml(content);
                return newdoc;
            }
            else
            {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(response.Content);
                return doc;
            }

        }
    }
    public class sessionObj
    {
        public string SequenceNumber { get; set; }
        public bool isStateFull { get; set; }
        public string SecurityToken { get; set; }
        public bool InSeries { get; set; }
        public bool End { get; set; }
        public string SessionId { get; set; }
    }
    public class pricetobeat
    {
        public string city { get; set; }
        public string price { get; set; }
        public string type { get; set; }
        public string cabin { get; set; }
    }
    public class MPConfig
    {
        public string searchMonth { get; set; }
        public string holiday { get; set; }
        public int noOfAdults { get; set; }
    }
}
