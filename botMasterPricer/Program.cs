﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;

namespace botMasterPricer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (checkRun()) {
                runMasterPricer();
            }
        }
        static bool checkRun (){
            Process[] processlist = Process.GetProcesses();
            Process currentProcess = Process.GetCurrentProcess();
            bool isrun = false;
            foreach (Process theprocess in processlist)
            {
                if (theprocess.ProcessName == currentProcess.ProcessName)
                {
                    isrun = true;
                    break;
                }
            }

            return isrun;
        }
        static void runMasterPricer() {
            try
            {
                Function_Utilities func = new Function_Utilities();
                string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
                string lastrun = ConfigurationManager.AppSettings["masterpricerSH_lastRun"].ToString();
                string currendDate = DateTime.Now.ToString("ddMMyyyy");
                if (lastrun != "")
                {
                    if (lastrun == currendDate)
                    {
                        return;
                    }
                }

                if (ConfigurationManager.AppSettings["Status_bot_master"].ToString().Equals("1"))
                {
                    ConfigurationManager.AppSettings["Status_bot_master"] = "0";
                    List<pricetobeat> listpricetobeat = func.convertPriceToBeat(func.getResultFromQuery("select * from dbo.Bot_MasterPricer_priceToBeat order by id",
                                                                                                    connectionstring));
                    DateTime today = DateTime.Now;
                    MPConfig conf = func.getBotMasterPricerConfig(today, connectionstring);
                    func.setCultureInfowithPattern("yyyy-MM-dd", "en-GB");
                    func.updateMPTax(connectionstring);

                    for (int i = 0; i < listpricetobeat.Count; i++)
                    {
                        pricetobeat prtobt = listpricetobeat[i];
                        func.deleteOldDataOfMasterPricer(prtobt, connectionstring);

                        List<string> typeMaster = func.splitStringwithString(prtobt.type, ',');
                        bool isconnect = false, isdirect = false, isnonstop = false;
                        string typemaster = "";

                        if (typeMaster.Contains("23")) isnonstop = true; typemaster = "23";
                        if (typeMaster.Contains("24")) isconnect = true; typemaster = "24";
                        if (typeMaster.Contains("26")) isdirect = true; typemaster = "26";

                        List<string> cabin = func.splitStringwithString(prtobt.cabin, ',');

                        foreach (string cabclass in cabin)
                        {
                            XmlDocument doc = func.callMasterPricerwithAPI(conf.noOfAdults,
                                                                prtobt.price,
                                                                prtobt.city,
                                                                conf.searchMonth,
                                                                conf.holiday,
                                                                cabclass,
                                                                isnonstop,
                                                                isdirect,
                                                                isconnect);

                            if (!doc.InnerXml.Contains("ErrorCode"))
                            {
                                xmlReader(doc.InnerXml,
                                      prtobt,
                                      conf,
                                      typemaster,
                                      func.convertCabinClass(cabclass),
                                      cabclass);
                                Console.WriteLine("Done.1");
                            }
                            else
                            {
                                continue;
                            }

                        }

                    }
                    ConfigurationManager.AppSettings["masterpricerSH_lastRun"] = currendDate;
                    ConfigurationManager.AppSettings["Status_bot_master"] = "1";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ConfigurationManager.AppSettings["masterpricerSH_Error"] = ConfigurationManager.AppSettings["masterpricerSH_Error"].ToString() + "</br>" + ex.Message;
                ConfigurationManager.AppSettings["Status_bot_master"] = "1";
            }
        }
        private static void xmlreaderFlightGo(string objectAsXmlString, int noOfAdults, string tripmaster, string typeMasterPricer, string cabclass, string city)
        {
            Function_Utilities func = new Function_Utilities();
            string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(objectAsXmlString);
            string xpath_groupOfFlights = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/flightIndex[1]/groupOfFlights";
            var nodes_groupOfFlights = xmlDoc.SelectNodes(xpath_groupOfFlights);
            int ii_groupOfFlights = 1;
            foreach (XmlNode childrenNode_groupOfFlights in nodes_groupOfFlights)
            {
                string xpath_flightDetails = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails";
                var nodes_flightDetails = xmlDoc.SelectNodes(xpath_flightDetails);
                for (int i = 1; i <= 1; i++)
                {
                    string locIdBack = "";
                    if (nodes_flightDetails.Count == 1)
                    {
                        locIdBack = childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[1]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails.Count == 2)
                    {
                        locIdBack = childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[2]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails.Count == 3)
                    {
                        locIdBack = childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[3]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails.Count == 4)
                    {
                        locIdBack = childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[4]/flightInformation/location[2]/locationId").InnerText;
                    }

                    string cmdflightgo = "INSERT INTO [Bot_MasterPricer_flightGo] ([TripMaster],[TypeMasterPricer],[City],[cabin],[Datetime],[refNumberGo]," +
                        "[locationIdGO],[locationIdBack],[marketingCarrier],[flightNumber],noOfAdults)"
                        + " VALUES (" + func.addSingleQuote(tripmaster)
                        + "," + func.addSingleQuote(typeMasterPricer)
                        + "," + func.addSingleQuote(city)
                        + "," + func.addSingleQuote(cabclass)
                        + "," + func.addSingleQuote(DateTime.Now.ToString("yyyy-MM-dd"))
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/propFlightGrDetail/flightProposal[1]/ref").InnerText)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[" + i + "]/flightInformation/location[1]/locationId").InnerText)
                        + "," + func.addSingleQuote(locIdBack)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[" + i + "]/flightInformation/companyId/marketingCarrier").InnerText)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights.SelectSingleNode("//flightIndex[1]/groupOfFlights[" + ii_groupOfFlights + "]/flightDetails[" + i + "]/flightInformation/flightNumber").InnerText)
                        + "," + noOfAdults
                        + ")";

                    func.updateData(cmdflightgo, connectionstring);
                }
                ii_groupOfFlights++;
            }
        }
        private static void xmlreaderMasterPricer(string objectAsXmlString, pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass)
        {
            Function_Utilities func = new Function_Utilities();
            string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
            string xpath = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(objectAsXmlString);
            var nodes = xmlDoc.SelectNodes(xpath);
            int ii_Master = 1;
            foreach (XmlNode childrenNode in nodes)
            {
                string xpath_segmentFlightRef = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation[" + ii_Master + "]/segmentFlightRef";
                var nodes_segmentFlightRef = xmlDoc.SelectNodes(xpath_segmentFlightRef);

                for (int i = 1; i <= nodes_segmentFlightRef.Count; i++)
                {
                    string xpath_CodeShareDetails = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/codeShareDetails";
                    var nodes_CodeShareDetails = xmlDoc.SelectNodes(xpath_CodeShareDetails);
                    var refback = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/segmentFlightRef[" + i + "]/referencingDetail[2]/refNumber");
                    string refbackVal = "";
                    if (refback != null)
                    {
                        refbackVal = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/segmentFlightRef[" + i + "]/referencingDetail[2]/refNumber").InnerText;
                    }

                    string text_codeshare = "";
                    for (int codeshare = 1; codeshare <= nodes_CodeShareDetails.Count; codeshare++)
                    {
                        string text = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/codeShareDetails[" + codeshare + "]/company").InnerText;
                        if (codeshare == nodes_CodeShareDetails.Count)
                        {
                            text_codeshare = text_codeshare + text;
                        }
                        else
                        {
                            text_codeshare = text_codeshare + text + ",";
                        }
                    }

                    string traveller = "";

                    if (conf.noOfAdults == 1)
                    {
                        traveller = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxReference/traveller[1]/ref").InnerText;
                    }
                    else if (conf.noOfAdults == 2)
                    {
                        var traveller1 = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxReference/traveller[1]/ref").InnerText;
                        var traveller2 = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxReference/traveller[2]/ref").InnerText;
                        traveller = traveller1 + "," + traveller2;
                    }

                    string xpath_Massage = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation[" + ii_Master + "]/paxFareProduct/fare";
                    var nodes_Massage = xmlDoc.SelectNodes(xpath_Massage);
                    string textSubjectQualifier = "";
                    string informationType = "";
                    string description = "";
                    int[] array_des = new int[4] { 4, 3, 1, 2 };
                    for (int i_massage = 0; i_massage < nodes_Massage.Count; i_massage++)
                    {

                        if (nodes_Massage.Count == 4)
                        {
                            array_des = new int[4] { 4, 3, 1, 2 };
                        }
                        if (nodes_Massage.Count == 3)
                        {
                            string desltd = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/fare[1]/pricingMessage/freeTextQualification/textSubjectQualifier").InnerText;
                            if (desltd == "LTD")
                            {
                                array_des = new int[3] { 3, 2, 1 };
                            }
                            else
                            {
                                array_des = new int[3] { 3, 1, 2 };
                            }
                        }
                        if (nodes_Massage.Count == 2)
                        {
                            string desltd = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/fare[1]/pricingMessage/freeTextQualification/textSubjectQualifier").InnerText;
                            if (desltd == "LTD")
                            {
                                array_des = new int[2] { 2, 1 };
                            }
                            else
                            {
                                array_des = new int[2] { 1, 2 };
                            }
                        }
                        if (nodes_Massage.Count == 1)
                        {
                            array_des = new int[1] { 1 };
                        }

                        string text_Sub = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/fare[" + array_des[i_massage] + "]/pricingMessage/freeTextQualification/textSubjectQualifier").InnerText;
                        string text_infor = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/fare[" + array_des[i_massage] + "]/pricingMessage/freeTextQualification/informationType").InnerText;

                        if (i_massage == nodes_Massage.Count - 1)
                        {
                            textSubjectQualifier = textSubjectQualifier + text_Sub;
                            informationType = informationType + text_infor;
                        }
                        else
                        {
                            textSubjectQualifier = textSubjectQualifier + text_Sub + ",";
                            informationType = informationType + text_infor + ",";
                        }
                        string xpath_description = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation[" + ii_Master + "]/paxFareProduct/fare[" + array_des[i_massage] + "]/pricingMessage/description";
                        var nodes_description = xmlDoc.SelectNodes(xpath_description);
                        for (int i_des = 1; i_des <= nodes_description.Count; i_des++)
                        {
                            string text_des = childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/fare[" + array_des[i_massage] + "]/pricingMessage/description[" + i_des + "]").InnerText;
                            description = description + text_des;
                        }
                        if (i_massage == nodes_Massage.Count - 1)
                        {
                            description = description + "";
                        }
                        else
                        {
                            description = description + ",";
                        }

                    }

                    var amountType = "";
                    var amount = "";
                    var currency = "";

                    string mpCommand = "INSERT INTO [Bot_MasterPricer_Master_Pricer] ([TripMaster],[TypeMasterPricer],[itemNumber],[City],[cabin],[Datetime],[TotalPrice],[TaxPrice],[refNumberGo],[refNumberBack],[paxFareNum],[TotalFareAmount],[TotalTaxAmount],[transportStageQualifier],[company],[ptc],[traveller],[textSubjectQualifier],[informationType],[description],[amountType],[amount],[currency],noOfAdults)"
                        + "VALUES ('R',"
                        + func.addSingleQuote(typemaster)
                        + "," + childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/itemNumber/itemNumberId/number").InnerText
                        + "," + func.addSingleQuote(prtbt.city)
                        + "," + func.addSingleQuote(cabclass)
                        + "," + func.addSingleQuote(DateTime.Now.ToString("yyyy-MM-dd"))
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/recPriceInfo[1]/monetaryDetail[1]").InnerText)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/recPriceInfo[1]/monetaryDetail[2]").InnerText)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/segmentFlightRef[" + i + "]/referencingDetail[1]/refNumber").InnerText)
                        + "," + func.addSingleQuote(refbackVal)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/paxFareNum").InnerText)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/totalFareAmount").InnerText)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/totalTaxAmount").InnerText)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxFareDetail/codeShareDetails[1]/transportStageQualifier").InnerText)
                        + "," + func.addSingleQuote(text_codeshare)
                        + "," + func.addSingleQuote(childrenNode.SelectSingleNode("//recommendation[" + ii_Master + "]/paxFareProduct/paxReference/ptc").InnerText)
                        + "," + func.addSingleQuote(traveller)
                        + "," + func.addSingleQuote(textSubjectQualifier)
                        + "," + func.addSingleQuote(informationType)
                        + "," + func.addSingleQuote(description)
                        + "," + func.addSingleQuote(amountType)
                        + "," + func.addSingleQuote(amount)
                        + "," + func.addSingleQuote(currency)
                        + "," + conf.noOfAdults + ")";

                    func.updateData(mpCommand, connectionstring);
                }
                ii_Master++;
            }
        }
        private static void xmlreaderFlightBack(string objectAsXmlString, int noOfAdults, string tripmaster, string typeMasterPricer, string cabclass, string city)
        {
            Function_Utilities func = new Function_Utilities();
            string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(objectAsXmlString);
            string xpath_groupOfFlights_back = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/flightIndex[2]/groupOfFlights";
            var nodes_groupOfFlights_back = xmlDoc.SelectNodes(xpath_groupOfFlights_back);
            int ii_groupOfFlights_back = 1;
            foreach (XmlNode childrenNode_groupOfFlights_back in nodes_groupOfFlights_back)
            {
                string xpath_flightDetails_back = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails";
                var nodes_flightDetails_back = xmlDoc.SelectNodes(xpath_flightDetails_back);
                for (int ii = 1; ii <= 1; ii++)
                {
                    string locIdBack = "";
                    if (nodes_flightDetails_back.Count == 1)
                    {
                        locIdBack = childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[1]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails_back.Count == 2)
                    {
                        locIdBack = childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[2]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails_back.Count == 3)
                    {
                        locIdBack = childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[3]/flightInformation/location[2]/locationId").InnerText;
                    }
                    else if (nodes_flightDetails_back.Count == 4)
                    {
                        locIdBack = childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[4]/flightInformation/location[2]/locationId").InnerText;
                    }

                    string cmdflightBack = "INSERT INTO [Bot_MasterPricer_flightBack] ([TripMaster],[TypeMasterPricer],[City],[cabin],[Datetime],[refNumberBack]," +
                            "[locationIdGO],[locationIdBack],[marketingCarrier],[flightNumber],noOfAdults)"
                        + " VALUES (" + func.addSingleQuote(tripmaster)
                        + "," + func.addSingleQuote(typeMasterPricer)
                        + "," + func.addSingleQuote(city)
                        + "," + func.addSingleQuote(cabclass)
                        + "," + func.addSingleQuote(DateTime.Now.ToString("yyyy-MM-dd"))
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/propFlightGrDetail/flightProposal[1]/ref").InnerText)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[" + ii + "]/flightInformation/location[1]/locationId").InnerText)
                        + "," + func.addSingleQuote(locIdBack)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[" + ii + "]/flightInformation/companyId/marketingCarrier").InnerText)
                        + "," + func.addSingleQuote(childrenNode_groupOfFlights_back.SelectSingleNode("//flightIndex[2]/groupOfFlights[" + ii_groupOfFlights_back + "]/flightDetails[" + ii + "]/flightInformation/flightNumber").InnerText)
                        + "," + noOfAdults + ")";

                    func.updateData(cmdflightBack, connectionstring);
                }
                ii_groupOfFlights_back++;
            }
        }
        private static void xmlreaderGroupOfFares(string objectAsXmlString, int noOfAdults, string tripmaster, string typeMasterPricer, string cabclass, string city)
        {
            Function_Utilities func = new Function_Utilities();
            string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(objectAsXmlString);
            string xpath_GroupOfFares = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation";
            var nodes_GroupOfFares = xmlDoc.SelectNodes(xpath_GroupOfFares);
            int ii_GroupOfFares = 1;
            foreach (XmlNode childrenNode_GroupOfFares in nodes_GroupOfFares)
            {
                string xpath_segmentFlightRef_GroupOfFares = "/Envelope/Body/Fare_MasterPricerTravelBoardSearchReply/recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares";
                var nodes_segmentFlightRef_GroupOfFares = xmlDoc.SelectNodes(xpath_segmentFlightRef_GroupOfFares);
                for (int i = 1; i <= 1; i++)
                {
                    string rbd = "";
                    var rbdgo = childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares[" + i + "]/productInformation/cabinProduct/rbd").InnerText;
                    var rbdback = childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[2]/groupOfFares[" + i + "]/productInformation/cabinProduct/rbd").InnerText;
                    rbd = rbdgo + "," + rbdback;

                    var avlStatus_go = childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares[" + i + "]/productInformation/cabinProduct/avlStatus").InnerText;
                    var avlStatus_back = childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[2]/groupOfFares[" + i + "]/productInformation/cabinProduct/avlStatus").InnerText;
                    string avlStatus = "";
                    avlStatus = avlStatus_go + "," + avlStatus_back;

                    string cmdgroupofFare = "INSERT INTO [Bot_MasterPricer_GroupOfFares] ([TripMaster],[TypeMasterPricer],[City],[Datetime],[itemNumber],[TyperefNumber],[rbd],[cabin],[avlStatus],[fareBasis],[passengerType],[fareType],noOfAdults)"
                        + " VALUES (" + func.addSingleQuote(tripmaster)
                        + "," + func.addSingleQuote(typeMasterPricer)
                        + "," + func.addSingleQuote(city)
                        + "," + func.addSingleQuote(DateTime.Now.ToString("yyyy-MM-dd"))
                        + "," + func.addSingleQuote(childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/itemNumber/itemNumberId/number").InnerText)
                        + "," + func.addSingleQuote("GO")
                        + "," + func.addSingleQuote(rbd)
                        + "," + func.addSingleQuote(cabclass)
                        + "," + func.addSingleQuote(avlStatus)
                        + "," + func.addSingleQuote(childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares[" + i + "]/productInformation/fareProductDetail/fareBasis").InnerText)
                        + "," + func.addSingleQuote(childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares[" + i + "]/productInformation/fareProductDetail/passengerType").InnerText)
                        + "," + func.addSingleQuote(childrenNode_GroupOfFares.SelectSingleNode("//recommendation[" + ii_GroupOfFares + "]/paxFareProduct/fareDetails[1]/groupOfFares[" + i + "]/productInformation/fareProductDetail/fareType").InnerText)
                        + "," + noOfAdults
                        + ")";

                    func.updateData(cmdgroupofFare, connectionstring);
                }
                ii_GroupOfFares++;
            }
        }
        private static void xmlReader(string objectAsXmlString, pricetobeat prtbt, MPConfig conf, string typemaster, string cabclass, string cabtag)
        {
            Function_Utilities func = new Function_Utilities();
            string connectionstring = ConfigurationManager.AppSettings["connectionstring"].ToString();
            xmlreaderMasterPricer(objectAsXmlString, prtbt, conf, typemaster, cabtag);
            xmlreaderFlightGo(objectAsXmlString, conf.noOfAdults, "R", typemaster, cabtag, prtbt.city);
            xmlreaderFlightBack(objectAsXmlString, conf.noOfAdults, "R", typemaster, cabtag, prtbt.city);
            xmlreaderGroupOfFares(objectAsXmlString, conf.noOfAdults, "R", typemaster, cabtag, prtbt.city);
            func.updateRecordFlightMP(prtbt, conf, typemaster, cabtag, connectionstring);
            func.updateAvgcity(prtbt, conf, typemaster, cabtag, connectionstring);
            func.updateRecordFareMP(prtbt, conf, typemaster, cabtag, connectionstring);
            func.updateSpecialFareOK(prtbt, conf, typemaster, cabtag, connectionstring);
        }
    }
}
